# TED

TED (Toggle LED) ist der neuste Hit auf den Schulhöfen. Das Gadget besteht aus 25 mit LED versehenen Tastern in 5 Reihen und 5 Spalten.

Beim Spiel sind zu Beginn einige zufällige Taster angeschaltet. Bei einem Spielzug kann ein Taster gedrückt werden. Das bewirkt, das diese LED und alle bis zu 4 benachbarten LED umgeschaltet werden, von an nach aus beziehungsweise von aus nach an. Ziel des Spieles ist, dass alle Taster ausgeschaltet sind.

Implementieren Sie das Spiel unter Berücksichtigung des MVC-Patterns.
